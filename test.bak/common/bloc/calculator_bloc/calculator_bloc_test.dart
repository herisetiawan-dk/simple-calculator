import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';

void main() {
  CalculatorBloc calculatorBloc;

  setUp(() {
    calculatorBloc = CalculatorBloc();
  });

  tearDown(() {
    calculatorBloc?.close();
  });
  group('Calculator Bloc', () {
    blocTest(
      'should add value to result when trigger [CalculatorAddNumberEvent] '
      'and have [CalculatorResultState] as state',
      build: () => calculatorBloc,
      act: (bloc) {
        bloc.add(CalculatorAddNumberEvent(0));
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '0.0');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
      ],
    );

    blocTest(
      'should add value to result when trigger [CalculatorAddNumberEvent] '
      'and have [CalculatorResultState] as state '
      'with number added between current result and coma',
      build: () => calculatorBloc,
      act: (bloc) {
        bloc
          ..add(CalculatorAddNumberEvent(3))
          ..add(CalculatorOperateEvent(Operator.division))
          ..add(CalculatorAddNumberEvent(2))
          ..add(CalculatorCountEvent())
          ..add(CalculatorAddNumberEvent(1));
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '11.5');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );

    blocTest(
      'should count current operation when called other operator'
      'and place result on [prevResult]',
      build: () => calculatorBloc,
      act: (bloc) {
        bloc
          ..add(CalculatorAddNumberEvent(3))
          ..add(CalculatorOperateEvent(Operator.division))
          ..add(CalculatorAddNumberEvent(2))
          ..add(CalculatorOperateEvent(Operator.addition));
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.prevResult, '1.5');
        expect(bloc.state.result, '0');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );

    blocTest(
      'should delete last number when given [CalculatorEraseEvent] event',
      build: () => calculatorBloc,
      act: (bloc) {
        bloc
          ..add(CalculatorAddNumberEvent(3))
          ..add(CalculatorAddNumberEvent(2))
          ..add(CalculatorAddNumberEvent(1))
          ..add(CalculatorAddNumberEvent(2))
          ..add(CalculatorEraseEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '321.0');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );
    blocTest(
      'should delete last number behind coma when not zero '
      'when given [CalculatorEraseEvent] event',
      build: () => calculatorBloc,
      act: (bloc) {
        bloc
          ..add(CalculatorAddNumberEvent(1))
          ..add(CalculatorAddNumberEvent(2))
          ..add(CalculatorOperateEvent(Operator.division))
          ..add(CalculatorAddNumberEvent(5))
          ..add(CalculatorCountEvent())
          ..add(CalculatorEraseEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '2.0');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );
    blocTest(
      'should replace result with 0 when just left 1 character '
      'when given [CalculatorEraseEvent] event',
      build: () => calculatorBloc,
      act: (bloc) {
        bloc..add(CalculatorAddNumberEvent(1))..add(CalculatorEraseEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '0');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );
    blocTest(
      'should reset state to [CalculatorInitState] '
      'when called [CalculatorCleanEvent] event',
      build: () => calculatorBloc,
      act: (bloc) {
        bloc
          ..add(CalculatorAddNumberEvent(1))
          ..add(CalculatorEraseEvent())
          ..add(CalculatorCleanEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '0');
        expect(bloc.state.prevOperation, isNull);
        expect(bloc.state.prevResult, '');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorInitState>(),
      ],
    );
    blocTest(
      'should move result value to prevResult when call [CalculatorCountEvent] '
      'when operation is empty, then set result to 0',
      build: () => calculatorBloc,
      act: (bloc) {
        bloc..add(CalculatorAddNumberEvent(1))..add(CalculatorCountEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '0');
        expect(bloc.state.prevOperation, isNull);
        expect(bloc.state.prevResult, '1.0');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );
    blocTest(
      'should addition a to b when user call [CalculatorOperateEvent] '
      'with [Operator.addition] then call event [CalculatorCountEvent]',
      build: () => calculatorBloc,
      act: (CalculatorBloc bloc) {
        final int a = 1;
        final int b = 2;
        bloc
          ..add(CalculatorAddNumberEvent(a))
          ..add(CalculatorOperateEvent(Operator.addition))
          ..add(CalculatorAddNumberEvent(b))
          ..add(CalculatorCountEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '3.0');
        expect(bloc.state.prevOperation, isNull);
        expect(bloc.state.prevResult, '');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );
    blocTest(
      'should substract a with b when user call [CalculatorOperateEvent] '
      'with [Operator.addition] then call event [CalculatorCountEvent]',
      build: () => calculatorBloc,
      act: (CalculatorBloc bloc) {
        final int a = 5;
        final int b = 3;
        bloc
          ..add(CalculatorAddNumberEvent(a))
          ..add(CalculatorOperateEvent(Operator.substract))
          ..add(CalculatorAddNumberEvent(b))
          ..add(CalculatorCountEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '2.0');
        expect(bloc.state.prevOperation, isNull);
        expect(bloc.state.prevResult, '');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );
    blocTest(
      'should multiply a with b when user call [CalculatorOperateEvent] '
      'with [Operator.addition] then call event [CalculatorCountEvent]',
      build: () => calculatorBloc,
      act: (CalculatorBloc bloc) {
        final int a = 4;
        final int b = 2;
        bloc
          ..add(CalculatorAddNumberEvent(a))
          ..add(CalculatorOperateEvent(Operator.multiply))
          ..add(CalculatorAddNumberEvent(b))
          ..add(CalculatorCountEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '8.0');
        expect(bloc.state.prevOperation, isNull);
        expect(bloc.state.prevResult, '');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );
    blocTest(
      'should divide a with b when user call [CalculatorOperateEvent] '
      'with [Operator.addition] then call event [CalculatorCountEvent]',
      build: () => calculatorBloc,
      act: (CalculatorBloc bloc) {
        final int a = 4;
        final int b = 2;
        bloc
          ..add(CalculatorAddNumberEvent(a))
          ..add(CalculatorOperateEvent(Operator.division))
          ..add(CalculatorAddNumberEvent(b))
          ..add(CalculatorCountEvent());
      },
      verify: (CalculatorBloc bloc) {
        expect(bloc.state.result, '2.0');
        expect(bloc.state.prevOperation, isNull);
        expect(bloc.state.prevResult, '');
      },
      expect: <dynamic>[
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
        isA<CalculatorResultState>(),
      ],
    );
  });
}
