import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:simple_calculator/common/injector/injector.dart';
import 'package:simple_calculator/domain/entities/calculator_history_entity.dart';
import 'package:simple_calculator/domain/repositories/__mocks__/calculator_repository_mock.dart';
import 'package:simple_calculator/domain/repositories/calculator_repository.dart';
import 'package:simple_calculator/domain/usecases/calclulator_usecase.dart';

void main() {
  CalculatorRepository calculatorRepository;
  CalculatorUsecase calculatorUsecase;

  setUp(() {
    Injector.container
        .registerInstance<CalculatorRepository>(MockCalculatorRepository());
    calculatorRepository = Injector.resolve<CalculatorRepository>();
    calculatorUsecase = CalculatorUsecase(
      calculatorRepository: calculatorRepository,
    );
  });

  tearDown(() {
    Injector.container?.clear();
  });
  group('Calculator UseCase ', () {
    test('should store to local when calling function [storeHistoryToLocal]',
        () async {
      //Given
      bool called = false;
      final CalculatorHistoryEntity calculatorHistoryEntity =
          CalculatorHistoryEntity();
      when(calculatorRepository.storeHistoryToLocal(any)).thenAnswer((_) async {
        called = true;
      });

      //When
      await calculatorUsecase.storeHistoryToLocal(calculatorHistoryEntity);

      //Then
      expect(called, isTrue);
      verify(calculatorRepository.storeHistoryToLocal(any)).called(1);
    });

    test('should get all history when calling function [getAllHistories]',
        () async {
      //Given
      bool called = false;
      when(calculatorRepository.getAllHistories()).thenAnswer((_) async => []);

      //When
      await calculatorUsecase.getAllHistories();

      //Then
      expect(called, isTrue);
      verify(calculatorRepository.getAllHistories()).called(1);
    });

    test('should delete history when calling function [deleteHistory]',
        () async {
      //Given
      bool called = false;
      const String key = 'key';
      when(calculatorRepository.deleteHistory(key)).thenAnswer((_) async {
        called = true;
      });

      //When
      await calculatorUsecase.deleteHistory(key);

      //Then
      expect(called, isTrue);
      verify(calculatorRepository.deleteHistory(key)).called(1);
    });

    test('should delete all history when calling function [cleanAllHistory]',
        () async {
      //Given
      bool called = false;
      when(calculatorRepository.cleanAllHistory()).thenAnswer((_) async {
        called = true;
      });

      //When
      await calculatorUsecase.cleanAllHistory();

      //Then
      expect(called, isTrue);
      verify(calculatorRepository.cleanAllHistory()).called(1);
    });
  });
}
