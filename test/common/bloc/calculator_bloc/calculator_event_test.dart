import 'package:flutter_test/flutter_test.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/calculator_event.dart';

void main() {
  group('Calculator Event ', () {
    test('should return hashCode', () {
      expect(CalculatorAddNumberEvent(1).hashCode, isA<int>());
    });
  });
}
