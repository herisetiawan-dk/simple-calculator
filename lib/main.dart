import 'package:flutter/material.dart';
import 'package:simple_calculator/common/injector/injector.dart';
import 'package:simple_calculator/common/utils/database_utils.dart';
import 'package:simple_calculator/presentation/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DatabaseUtil.initDatabase();
  Injector.setup();

  runApp(App());
}
