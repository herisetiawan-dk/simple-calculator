// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'injector.dart';

// **************************************************************************
// KiwiInjectorGenerator
// **************************************************************************

class _$Injector extends Injector {
  @override
  void _configureBlocs() {
    final KiwiContainer container = KiwiContainer();
    container.registerFactory(
        (c) => CalculatorBloc(calculatorUsecase: c<CalculatorUsecase>()));
  }

  @override
  void _configureUsecases() {
    final KiwiContainer container = KiwiContainer();
    container.registerSingleton((c) =>
        CalculatorUsecase(calculatorRepository: c<CalculatorRepository>()));
  }

  @override
  void _configureRepositories() {
    final KiwiContainer container = KiwiContainer();
    container.registerSingleton<CalculatorRepository>((c) =>
        CalculatorRepositoryImpl(
            calculatorHistoryLocalDataSource:
                c<CalculatorHistoryLocalDataSource>()));
  }

  @override
  void _configureLocalDataSources() {
    final KiwiContainer container = KiwiContainer();
    container.registerSingleton((c) => CalculatorHistoryLocalDataSource());
  }
}
