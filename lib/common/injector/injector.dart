import 'package:kiwi/kiwi.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/data/datasource/local/calculator_history_local_datasource.dart';
import 'package:simple_calculator/data/repositories/calculator_repository_impl.dart';
import 'package:simple_calculator/domain/repositories/calculator_repository.dart';
import 'package:simple_calculator/domain/usecases/calclulator_usecase.dart';

part 'injector.g.dart';

abstract class Injector {
  static KiwiContainer container = KiwiContainer();
  static void setup() {
    final injector = _$Injector();
    injector._configure();
  }

  static final resolve = container.resolve;

  void _configure() {
    _configureBlocs();
    _configureUsecases();
    _configureRepositories();
    _configureLocalDataSources();
  }

  @Register.factory(CalculatorBloc)
  void _configureBlocs();

  @Register.singleton(CalculatorUsecase)
  void _configureUsecases();

  @Register.singleton(CalculatorRepository, from: CalculatorRepositoryImpl)
  void _configureRepositories();

  @Register.singleton(CalculatorHistoryLocalDataSource)
  void _configureLocalDataSources();
}
