import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/data/datasource/local/databases/tables/calculator_history_table.dart';
import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';
import 'package:simple_calculator/domain/usecases/calclulator_usecase.dart';

class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  final CalculatorUsecase calculatorUsecase;
  CalculatorBloc({
    this.calculatorUsecase,
  }) : super(CalculatorInitState());
  @override
  Stream<CalculatorState> mapEventToState(CalculatorEvent event) async* {
    switch (event.runtimeType) {
      case CalculatorAddNumberEvent:
        yield* _mapAddNumberToState(event);
        break;
      case CalculatorOperateEvent:
        yield* _mapOperateToState(event);
        break;
      case CalculatorCountEvent:
        yield* _mapCountToState(event);
        break;
      case CalculatorEraseEvent:
        yield* _mapEraseToState(event);
        break;
      case CalculatorCleanEvent:
        yield* _mapCleanToState(event);
        break;
    }
  }

  Stream<CalculatorState> _mapAddNumberToState(
      CalculatorAddNumberEvent event) async* {
    final prevResult = double.parse(state.result).toString();
    final splitedResult = prevResult.split('.');
    String result;
    if (splitedResult.last == '0') {
      result = '${splitedResult.first}${event.value}';
    } else {
      result = '${splitedResult.first}${event.value}.${splitedResult.last}';
    }

    yield CalculatorResultState(
      prevResult: state.prevResult,
      result: double.parse(result).toString(),
      prevOperation: state.prevOperation,
    );
  }

  Stream<CalculatorState> _mapOperateToState(
      CalculatorOperateEvent event) async* {
    if (state.prevOperation == null && state.prevResult.isEmpty) {
      yield CalculatorResultState(
        prevResult: state.result,
        result: '0',
        prevOperation: event.operation,
      );
    } else {
      final result = _counter(double.parse(state.prevResult),
          double.parse(state.result), state.prevOperation);
      yield CalculatorResultState(
        prevResult: result.toString(),
        result: '0',
        prevOperation: event.operation,
      );
    }
  }

  Stream<CalculatorState> _mapCountToState(CalculatorCountEvent event) async* {
    if (state.prevOperation == null && state.prevResult.isEmpty) {
      yield CalculatorResultState(
        prevResult: state.result,
        result: '0',
      );
    } else {
      final result = _counter(double.parse(state.prevResult),
          double.parse(state.result), state.prevOperation);
      final history = CalculatorHistoryTable(
        firstNumber: state.prevResult,
        secondNumber: state.result,
        operation: state.prevOperation.text,
        result: result.toString(),
        timestamp: DateTime.now().toIso8601String(),
      );
      yield CalculatorResultState(
        prevResult: '',
        result: result.toString(),
      );
      await calculatorUsecase.storeHistoryToLocal(history);
      event.callback?.call();
    }
  }

  Stream<CalculatorState> _mapEraseToState(CalculatorEraseEvent event) async* {
    final result = state.result.replaceAll('.0', '').split('');

    if (result.length == 1) {
      yield CalculatorResultState(
        prevResult: state.prevResult,
        prevOperation: state.prevOperation,
        result: '0',
      );
    } else {
      result.removeLast();
      if (result.last == '.') {
        result.removeLast();
      }

      yield CalculatorResultState(
        prevResult: state.prevResult,
        prevOperation: state.prevOperation,
        result: double.parse(result.join()).toString(),
      );
    }
  }

  Stream<CalculatorState> _mapCleanToState(CalculatorCleanEvent event) async* {
    yield CalculatorInitState();
  }

  double _counter(double a, double b, Operator operation) {
    switch (operation) {
      case Operator.addition:
        return a + b;
      case Operator.substract:
        return a - b;
      case Operator.multiply:
        return a * b;
      case Operator.division:
        return a / b;
      default:
        return 0;
    }
  }
}
