import 'package:flutter/material.dart';
import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';

class CalculatorConstants {
  static const ValueKey<String> oneKey = ValueKey('1');
  static const ValueKey<String> twoKey = ValueKey('2');
  static const ValueKey<String> threeKey = ValueKey('3');
  static const ValueKey<String> fourKey = ValueKey('4');
  static const ValueKey<String> fiveKey = ValueKey('5');
  static const ValueKey<String> sixKey = ValueKey('6');
  static const ValueKey<String> sevenKey = ValueKey('7');
  static const ValueKey<String> eightKey = ValueKey('8');
  static const ValueKey<String> nineKey = ValueKey('9');
  static const ValueKey<String> zeroKey = ValueKey('0');
  static const ValueKey<String> additionKey = ValueKey('addition_key');
  static const ValueKey<String> substractKey = ValueKey('substract_key');
  static const ValueKey<String> multiplyKey = ValueKey('multiply_key');
  static const ValueKey<String> divisionKey = ValueKey('division_key');
  static const ValueKey<String> deleteKey = ValueKey('delete_key');
  static const ValueKey<String> cleanKey = ValueKey('clean_key');
  static const ValueKey<String> resultKey = ValueKey('result_key');

  static List<List<CalculatorEntity>> getItem = [
    [
      CalculatorEntity(
        key: resultKey,
        title: '=',
        bgColor: Colors.teal,
        operation: Operator.result,
        flex: 2,
      ),
      CalculatorEntity(
        key: cleanKey,
        title: 'AC',
        bgColor: Colors.teal,
        operation: Operator.clean,
      ),
      CalculatorEntity(
        key: deleteKey,
        icon: Icons.delete,
        bgColor: Colors.teal,
        operation: Operator.erase,
      ),
    ],
    [
      CalculatorEntity(
        key: sevenKey,
        title: '7',
        value: 7,
      ),
      CalculatorEntity(
        key: eightKey,
        title: '8',
        value: 8,
      ),
      CalculatorEntity(
        key: nineKey,
        title: '9',
        value: 9,
      ),
      CalculatorEntity(
        key: multiplyKey,
        title: '*',
        bgColor: Colors.teal,
        operation: Operator.multiply,
      ),
    ],
    [
      CalculatorEntity(
        key: fourKey,
        title: '4',
        value: 4,
      ),
      CalculatorEntity(
        key: fiveKey,
        title: '5',
        value: 5,
      ),
      CalculatorEntity(
        key: sixKey,
        title: '6',
        value: 6,
      ),
      CalculatorEntity(
        key: substractKey,
        title: '-',
        bgColor: Colors.teal,
        operation: Operator.substract,
      ),
    ],
    [
      CalculatorEntity(
        key: oneKey,
        title: '1',
        value: 1,
      ),
      CalculatorEntity(
        key: twoKey,
        title: '2',
        value: 2,
      ),
      CalculatorEntity(
        key: threeKey,
        title: '3',
        value: 3,
      ),
      CalculatorEntity(
        key: additionKey,
        title: '+',
        bgColor: Colors.teal,
        operation: Operator.addition,
      ),
    ],
    [
      CalculatorEntity(
        key: zeroKey,
        title: '0',
        value: 0,
        flex: 3,
      ),
      CalculatorEntity(
        key: divisionKey,
        title: '/',
        bgColor: Colors.teal,
        operation: Operator.division,
      ),
    ],
  ];
}
