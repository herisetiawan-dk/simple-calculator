import 'package:simple_calculator/domain/entities/calculator_history_entity.dart';

class CalculatorHistoryModel extends CalculatorHistoryEntity {
  CalculatorHistoryModel({
    String firstNumber,
    String secondNumber,
    String operation,
    String result,
    String timestamp,
  }) : super(
          firstNumber: firstNumber,
          secondNumber: secondNumber,
          operation: operation,
          result: result,
          timestamp: timestamp,
        );
}
