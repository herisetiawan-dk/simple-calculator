import 'package:simple_calculator/common/constants/hive_boxname_constants.dart';
import 'package:simple_calculator/common/utils/database_utils.dart';
import 'package:simple_calculator/data/datasource/local/base_local_datasource.dart';
import 'package:simple_calculator/data/datasource/local/databases/tables/calculator_history_table.dart';
import 'package:simple_calculator/data/models/calculator_history_model.dart';

class CalculatorHistoryLocalDataSource extends BaseLocalDataSource<
    CalculatorHistoryTable, CalculatorHistoryModel> {
  CalculatorHistoryLocalDataSource()
      : super(boxName: HiveBoxNameConstants.calculatorHistory) {
    DatabaseUtil.registerAdapter<CalculatorHistoryTable>(
        CalculatorHistoryTableAdapter());
  }
}
