import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:simple_calculator/common/utils/database_utils.dart';

abstract class BaseLocalDataSource<TableType, TableModel> {
  String _boxName;
  Future<Box<TableType>> boxInstance;

  BaseLocalDataSource({
    @required String boxName,
  }) {
    _boxName = boxName;
    boxInstance = DatabaseUtil.getBox<TableType>(boxName: _boxName);
  }

  Future<Box<TableType>> get getBoxInstance async => _openBox();

  Future<Box<TableType>> _openBox() => boxInstance;

  Future<TableType> get(String key) async {
    final Box<TableType> box = await _openBox();

    return box.get(key);
  }

  Future<List<TableType>> getAll() async {
    final box = await _openBox();
    return box.values.toList();
  }

  Future<void> put(String key, TableType value) async {
    final box = await _openBox();
    box.put(key, value);
  }

  Future<void> detele(String key) async {
    final box = await _openBox();
    box.delete(key);
  }

  Future<void> deteleAll() async {
    final box = await _openBox();
    box.clear();
  }
}
