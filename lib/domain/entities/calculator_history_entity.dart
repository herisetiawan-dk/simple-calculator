import 'package:hive/hive.dart';

class CalculatorHistoryEntity {
  @HiveField(0)
  String firstNumber;
  @HiveField(1)
  String secondNumber;
  @HiveField(2)
  String operation;
  @HiveField(3)
  String result;
  @HiveField(4)
  String timestamp;

  CalculatorHistoryEntity({
    this.firstNumber,
    this.secondNumber,
    this.operation,
    this.result,
    this.timestamp,
  });
}
