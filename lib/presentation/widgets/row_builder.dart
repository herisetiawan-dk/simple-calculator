import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';

class RowBuilder extends StatelessWidget {
  final List<CalculatorEntity> items;
  final void Function() resultCallback;

  const RowBuilder({Key key, this.items, this.resultCallback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: items
          .map((item) => Expanded(
                flex: item.flex,
                child: LayoutBuilder(
                  builder: (context, constraints) => Padding(
                    padding: const EdgeInsets.all(1.0),
                    child: InkWell(
                      onTap: () => _onTapButton(context, item),
                      child: Container(
                        key: item.key,
                        height: constraints.maxWidth / item.flex,
                        width: constraints.maxWidth,
                        color: item.bgColor ?? Theme.of(context).primaryColor,
                        child: Center(
                          child: item.title != null
                              ? Text(
                                  item.title,
                                  style: TextStyle(
                                    color: item.color ?? Colors.white,
                                    fontSize: 20,
                                  ),
                                )
                              : Icon(
                                  item.icon,
                                  color: item.color ?? Colors.white,
                                ),
                        ),
                      ),
                    ),
                  ),
                ),
              ))
          .toList(),
    );
  }

  void _onTapButton(BuildContext context, CalculatorEntity item) {
    CalculatorEvent event;
    if (item.operation == null) {
      event = CalculatorAddNumberEvent(item.value);
    } else {
      switch (item.operation) {
        case Operator.result:
          event = CalculatorCountEvent(resultCallback?.call);
          break;
        case Operator.erase:
          event = CalculatorEraseEvent();
          break;
        case Operator.clean:
          event = CalculatorCleanEvent();
          break;
        default:
          event = CalculatorOperateEvent(item.operation);
      }
    }
    BlocProvider.of<CalculatorBloc>(context).add(event);
  }
}
