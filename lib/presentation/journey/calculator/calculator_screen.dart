import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/presentation/widgets/column_builder.dart';
import 'package:simple_calculator/common/constants/calculator_constants.dart';

class CalculatorScreen extends StatelessWidget {
  final CalculatorBloc calculatorBloc;
  CalculatorScreen({this.calculatorBloc});
  @override
  Widget build(BuildContext context) {
    final calculatorItem = CalculatorConstants.getItem;

    return BlocProvider<CalculatorBloc>(
      create: (context) => calculatorBloc,
      child: Container(
        child: ColumnBuilder(
          calcItems: calculatorItem,
        ),
      ),
    );
  }
}
